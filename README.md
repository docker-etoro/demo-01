# Docker Workshop
Demo 01: Our first contact with docker

---

## Preparations

 - Pull the jenkins image:

```
$ docker pull jenkins/jenkins
```

## Instructions

 - Check the current running containers and images:
```
$ docker ps
```
```
$ docker images
```

 - Browse to the port 8080 to show to show that it's empty:
```
http://server-ip:8080
```

 - Run a Jenkins container
```
$ docker run --name jenkins -p 8080:8080 jenkins/jenkins
```

 - Browse to the Jenkins application
```
http://server-ip:8080
```

 - Look up the administrator password in the log:
```
*************************************************************
*************************************************************
*************************************************************  
Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:  
f7702d8c3e204cf7bfcce76dea9e1ec0  
This may also be found at: /var/jenkins_home/secrets/initialAdminPassword  
*************************************************************
*************************************************************
*************************************************************
```

 - Configure Jenkins using the administration password
 
 - Exit and stop the container by using "Ctrl + C"
 
 - Remove the stopped container
 
 ```
 $ docker rm -f jenkins
 ```
